using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class ScreenShot : MonoBehaviour
{
    public int superSize = 2;
    int shotIndex = 0;
    AudioSource cameraSound;
    public GameObject UI;
    // Start is called before the first frame update
    void Start()
    {
        cameraSound = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Screenshot()
    {
        UI.SetActive(false);
        cameraSound.Play();
        StartCoroutine(TakeScreenShot());
    }

    IEnumerator TakeScreenShot()
    {
        yield return new WaitForEndOfFrame();
        Texture2D texture = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
        texture.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        texture.Apply();
        string name = "Screenshot" + shotIndex.ToString() + ".png";
        NativeGallery.SaveImageToGallery(texture, "Evento Compensar", name);
        //ScreenCapture.CaptureScreenshot($"Screenshot{ shotIndex}.png", superSize);
        //byte[] bytes = texture.EncodeToPNG();
        //File.WriteAllBytes(Application.dataPath + "/Screenshot.png", bytes);
        shotIndex++;
        yield return new WaitForSeconds(1);
        UI.SetActive(true);
    }
}
