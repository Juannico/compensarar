using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Box : MonoBehaviour
{
    [HideInInspector] public bool hasbeentouched;
    Animator anim;
    public GameObject picture;
    public MeshRenderer[] childMeshes;
    AudioSource sound;
    public ParticleSystem part;
    public GameObject hashtag;
    // Start is called before the first frame update
    void Start()
    {
        hashtag.SetActive(false);
        sound = GetComponent<AudioSource>();
        //childMeshes = GetComponentsInChildren<MeshRenderer>();
        picture.SetActive(false);
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (hasbeentouched)
        {
            hasbeentouched = false;
            anim.SetTrigger("open");
            StartCoroutine(OpeningSequence());
        }
    }

    IEnumerator OpeningSequence()
    {
        yield return new WaitForSeconds(1);
        sound.Play();
        part.Play();
        hashtag.SetActive(true);
        picture.SetActive(true);
        foreach (MeshRenderer child in childMeshes)
        {
            print("gone");
            child.enabled = false;
        }
    }

    public void ForceMeshOut()
    {
        foreach (MeshRenderer child in childMeshes)
        {
            print("gone");
            child.enabled = false;
            //child.gameObject.SetActive(false);
        }
    }
}
