using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem.EnhancedTouch;
using UnityEngine.Events;

public class Swiping : MonoBehaviour
{
    public GameObject[] pictures;
    public bool finished;
    public ParticleSystem part;
    AudioSource sound;
    bool playonce;
    public GameObject hashtagtext;
    private void Start()
    {
        hashtagtext.SetActive(false);
        sound = GetComponent<AudioSource>();
        foreach (GameObject pic in pictures)
        {
            pic.SetActive(false);
        }
    }
    private void Update()
    {
        if (finished)
        {
            StartCoroutine(Finish());
        }
    }

    IEnumerator Finish()
    {
        yield return new WaitForSeconds(1);
        if (!playonce)
        {
            hashtagtext.SetActive(true);
            part.Play();
            sound.Play();
            playonce = true;
        }
        foreach (GameObject pic in pictures)
        {
            pic.SetActive(true);
        }
    }
    /*public bool IsSwiping;
    private Vector2 startTouchPosition;
    private Vector2 currentTouchPosition;
    public GameObject fallableObject;
    bool enabledTarget;
    //public int state;//0 modelo no cargado, 1 modelo cargado, 2 rigidbody activado, 3 objeto finalizado-ca�do, 4 modelo vuelve para no cargado
                     //public Rigidbody rb;

    private void OnEnable()
    {
        TouchSimulation.Enable();
        EnhancedTouchSupport.Enable();
        UnityEngine.InputSystem.EnhancedTouch.Touch.onFingerDown += OnTap;
        UnityEngine.InputSystem.EnhancedTouch.Touch.onFingerMove += OnSwipe;
        UnityEngine.InputSystem.EnhancedTouch.Touch.onFingerUp += OnCancelSwipe;
    }
    private void OnDisable()
    {
        TouchSimulation.Disable();
        EnhancedTouchSupport.Disable();
        UnityEngine.InputSystem.EnhancedTouch.Touch.onFingerDown -= OnTap;
        UnityEngine.InputSystem.EnhancedTouch.Touch.onFingerMove -= OnSwipe;
        UnityEngine.InputSystem.EnhancedTouch.Touch.onFingerUp -= OnCancelSwipe;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (enabledTarget)
        {
            print("enabled target");
            if (!IsSwiping) return;
            Ray raycast = Camera.main.ScreenPointToRay(currentTouchPosition);
            RaycastHit hit;
            if (Physics.Raycast(raycast, out hit))
            {
                print("raycast hit");
                if (hit.collider == null) return;
                fallableObject.GetComponent<Animator>().SetTrigger("fall");
                if (hit.rigidbody == null) return;

                hit.rigidbody.AddTorque(hit.point);
                hit.rigidbody.AddForce(hit.point - Camera.main.ScreenToWorldPoint(startTouchPosition));
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision == null) return;
        if (collision.collider == null) return;

        if (collision.collider.GetComponent<DetectionPlane>() != null)
        {
            //Agregar aqui lo que quiera que pase cuando el pin caiga que no pueda hacerse con eventos simples de Unity
            //OnPinFall?.Invoke();
        }
    }

    public void TargetFound()
    {
        enabledTarget = true;
    }
    public void TargetLost()
    {
        enabledTarget = false;
    }

    private void OnTap(Finger obj)
    {
        startTouchPosition = obj.screenPosition;
    }


    private void OnCancelSwipe(Finger obj)
    {
        IsSwiping = false;
    }

    private void OnSwipe(Finger obj)
    {
        IsSwiping = true;
        currentTouchPosition = obj.screenPosition;
    }*/
}
