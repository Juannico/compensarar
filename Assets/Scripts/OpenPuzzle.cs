using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OpenPuzzle : MonoBehaviour
{
    int interactLayer, boxlayer, multitap, bolo, balon, diente;
    // Start is called before the first frame update
    void Start()
    {
        interactLayer = LayerMask.GetMask("Button");
        boxlayer = LayerMask.GetMask("Box");
        multitap = LayerMask.GetMask("Multitap");
        bolo = LayerMask.GetMask("Bolo");
        balon = LayerMask.GetMask("Balon");
        diente = LayerMask.GetMask("Diente");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 1000f, interactLayer))
            {
                SceneManager.LoadScene(1);
            }
            else if (Physics.Raycast(ray, out hit, 1000f, boxlayer))
            {
                hit.transform.GetComponent<Box>().hasbeentouched = true;
            }
            else if (Physics.Raycast(ray, out hit, 1000f, multitap))
            {
                hit.transform.GetComponent<Multitap>().state++;
            }
            else if (Physics.Raycast(ray, out hit, 1000f, diente))
            {
                hit.transform.GetComponent<Animator>().SetTrigger("fall");
                hit.transform.GetComponent<Swiping>().finished = true;
            }
            else if (Physics.Raycast(ray, out hit, 1000f, balon))
            {
                hit.transform.GetComponent<Animator>().SetTrigger("fall");
                hit.transform.GetComponent<Swiping>().finished = true;
            }
            else if (Physics.Raycast(ray, out hit, 1000f, bolo))
            {
                hit.transform.GetComponent<Animator>().SetTrigger("fall");
                hit.transform.GetComponent<Swiping>().finished = true;
            }
        }
    }
}
