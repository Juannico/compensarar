using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderClass : MonoBehaviour
{
    public GameObject picture;
    public Slider slider;
    public AudioSource pop;
    public ParticleSystem fireworks;
    public GameObject text;
    bool playonce;
    // Start is called before the first frame update
    void Start()
    {
        picture.SetActive(false);
        slider.value = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (slider.value == 1)
        {
            if (!playonce)
            {
                text.GetComponent<TextMesh>().text = "#Elbienestaresmayor";
                slider.interactable = false;
                picture.SetActive(true);
                pop.Play();
                fireworks.Play();
            }
        }
    }
}
