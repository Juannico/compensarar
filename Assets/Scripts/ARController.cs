using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class ARController : MonoBehaviour
{
    GameObject[] labElements;
    //public Transform startPos;
    // Start is called before the first frame update
    void Start()
    {
        labElements = GameObject.FindGameObjectsWithTag("Lab");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void TargetFound()
    {
        foreach (GameObject lab in labElements)
        {
            lab.SetActive(true);
        }
    }

    public void TargetLost()
    {
        foreach (GameObject lab in labElements)
        {
            lab.SetActive(false);
        }
    }

}
