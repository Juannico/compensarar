using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Buttons : MonoBehaviour
{
    bool converted1, converted2, converted3, converted4, converted5, converted6;
    public GameObject picture;
    public GameObject b1, b2, b3, b4, b5, b6;
    public ParticleSystem part;
    bool playonce;
    public GameObject hashtag;
    // Start is called before the first frame update
    void Start()
    {
        hashtag.SetActive(false);
        picture.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (converted1 && converted2 && converted3 && converted4 && converted5 && converted6)
        {
            picture.SetActive(true);
            b1.SetActive(false);
            b2.SetActive(false);
            b3.SetActive(false);
            b4.SetActive(false);
            b5.SetActive(false);
            b6.SetActive(false);
            hashtag.SetActive(true);
            if (!playonce)
            {
                part.Play();
                playonce = true;
            }

        }   
    }

    public void ButtonTouch(int id)
    {
        switch (id)
        {
            case 1:
                converted1 = true;
                b1.GetComponent<Image>().color = Color.red;
                break;
            case 2:
                b2.GetComponent<Image>().color = Color.yellow;
                converted2 = true;
                break;
            case 3:
                b3.GetComponent<Image>().color = Color.blue;
                converted3 = true;
                break;
            case 4:
                b4.GetComponent<Image>().color = new Color(1, 0.5f, 0);
                converted4 = true;
                break;
            case 5:
                b5.GetComponent<Image>().color = Color.cyan;
                converted5 = true;
                break;
            case 6:
                b6.GetComponent<Image>().color = Color.green;
                converted6 = true;
                break;
        }
    }
}
