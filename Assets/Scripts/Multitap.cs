using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Multitap : MonoBehaviour
{
    public Renderer[] pictures;
    [HideInInspector] public int state;
    public GameObject hashtag;
    bool playonce;
    public ParticleSystem particle;
    AudioSource sound;
    // Start is called before the first frame update
    void Start()
    {
        sound = GetComponent<AudioSource>();
        hashtag.SetActive(false);
        //sound = GetComponent<AudioSource>();
        foreach (Renderer pic in pictures)
        {
            pic.material.color = Color.black;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(state == 1)
        {
            foreach (Renderer pic in pictures)
            {
                pic.material.color = new Color(0.1f, 0.1f, 0.1f);
            }
        }
        if(state == 2)
        {
            foreach (Renderer pic in pictures)
            {
                pic.material.color = new Color(0.2f, 0.2f, 0.2f);
            }
        }
        if (state == 3)
        {
            foreach (Renderer pic in pictures)
            {
                pic.material.color = new Color(0.3f, 0.3f, 0.3f);
            }
        }
        if (state == 4)
        {
            foreach (Renderer pic in pictures)
            {
                pic.material.color = new Color(0.4f, 0.4f, 0.4f);
            }
        }
        if (state == 5)
        {
            foreach (Renderer pic in pictures)
            {
                pic.material.color = new Color(0.5f, 0.5f, 0.5f);
            }
        }
        if (state == 6)
        {
            foreach (Renderer pic in pictures)
            {
                pic.material.color = new Color(0.6f, 0.6f, 0.6f);
            }
        }
        if (state == 7)
        {
            foreach (Renderer pic in pictures)
            {
                pic.material.color = new Color(0.7f, 0.7f, 0.7f);
            }
        }
        if (state == 8)
        {
            foreach (Renderer pic in pictures)
            {
                pic.material.color = new Color(0.8f, 0.8f, 0.8f);
            }
        }
        if (state == 9)
        {
            foreach (Renderer pic in pictures)
            {
                pic.material.color = new Color(0.9f, 0.9f, 0.9f);
            }
        }
        if (state >= 10)
        {
            if (!playonce)
            {
                hashtag.SetActive(true);
                sound.Play();
                particle.Play();
                playonce = true;
            }
            foreach (Renderer pic in pictures)
            {
                pic.material.color = Color.white;
            }
        }
    }
}
