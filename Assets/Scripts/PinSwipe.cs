using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem.EnhancedTouch;

/// <summary>
/// Para que funcione. Agregar de Packages el nuevo Input System
/// </summary>
public class PinSwipe : MonoBehaviour
{
    public bool IsSwiping;
    public UnityEvent OnPinFall;
    private Vector2 startTouchPosition;
    private Vector2 currentTouchPosition;
    private void OnEnable()
    {
        TouchSimulation.Enable();
        EnhancedTouchSupport.Enable();
        UnityEngine.InputSystem.EnhancedTouch.Touch.onFingerDown += OnTap;
        UnityEngine.InputSystem.EnhancedTouch.Touch.onFingerMove += OnSwipe;
        UnityEngine.InputSystem.EnhancedTouch.Touch.onFingerUp += OnCancelSwipe;
    }
    private void OnDisable()
    {
        TouchSimulation.Disable();
        EnhancedTouchSupport.Disable();
        UnityEngine.InputSystem.EnhancedTouch.Touch.onFingerDown -= OnTap;
        UnityEngine.InputSystem.EnhancedTouch.Touch.onFingerMove -= OnSwipe;
        UnityEngine.InputSystem.EnhancedTouch.Touch.onFingerUp -= OnCancelSwipe;
    }

    private void OnTap(Finger obj)
    {
        startTouchPosition = obj.screenPosition;
    }


    private void OnCancelSwipe(Finger obj)
    {
        IsSwiping = false;
    }

    private void OnSwipe(Finger obj)
    {
        IsSwiping = true;
        currentTouchPosition = obj.screenPosition;
    }

    // Update is called once per frame
    void Update()
    {
        if (!IsSwiping) return;
        Ray raycast = Camera.main.ScreenPointToRay(currentTouchPosition);
        RaycastHit hit;
        if (Physics.Raycast(raycast, out hit))
        {
            if (hit.rigidbody == null) return;

            hit.rigidbody.AddTorque(hit.point);
            hit.rigidbody.AddForce(hit.point - Camera.main.ScreenToWorldPoint(startTouchPosition));
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision == null) return;
        if (collision.collider == null) return;

        if (collision.collider.GetComponent<DetectionPlane>() != null)
        {
            //Agregar aqui lo que quiera que pase cuando el pin caiga que no pueda hacerse con eventos simples de Unity
            OnPinFall?.Invoke();
        }
    }
}
