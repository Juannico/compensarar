﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Memory : MonoBehaviour 
{
	int lastindex;
	GameObject Object1;
	GameObject Object2;
	public GameObject[] cartas = new GameObject[16];
	public GameObject[] front = new GameObject[16];
	public List<Texture> texturas = new List<Texture>(16);
	int seguimiento = 0;
	public static bool termine = false;
	public AudioSource audio1;
	public GameObject restart, reseteng, resetsp;
	//public static bool enjuego = false;
	// Use this for initialization
	void Start () 
	{
		for (int i = 0; i < 18; i++) 
		{
			int ramon = Random.Range (0, texturas.Count);
			cartas [i].GetComponentInChildren<Renderer>().material.mainTexture = texturas [ramon];
			front [i].tag = texturas [ramon].name;
			//front [1].tag = texturas [1].name;
			//Debug.Log (texturas [1].name);
			//Debug.Log (front [1].tag);
			texturas.RemoveAt (ramon);
		}
		//setup ();
	}
	// Update is called once per frame
	void Update ()
	{
		//Debug.Log (termine);
		if(Input.GetMouseButtonDown(0) && Manager.enjuego == true)
		{
			//termine = false;
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			RaycastHit raycast;
			if(Physics.Raycast(ray.origin,ray.direction,out raycast,Mathf.Infinity))
			{
				raycast.collider.gameObject.GetComponent<Animator> ().Play ("flip");
				audio1.Play ();
				if (Object1 == null) 
				{
					Object1 = raycast.collider.gameObject;
					Debug.Log (Object1.tag);
				} 
				else if (Object2 == null)
				{
					Object2 = raycast.collider.gameObject;
					Debug.Log (Object2.tag);
					if (Object1.tag == Object2.tag) 
					{
						seguimiento++;
						Object1 = null;
						Object2 = null;
					} 
					else 
					{
						Object1.GetComponent<Animator> ().Play ("flipback");
						audio1.Play ();
						Object2.GetComponent<Animator> ().Play ("flipback");
						audio1.Play ();
						Object1 = null;
						Object2 = null;
					}
				}
			}
		}
		if (seguimiento == 9) 
		{
			if (reseteng.activeSelf) 
			{
				reseteng.GetComponent<Image> ().enabled = true;
			}
			if (resetsp.activeSelf) 
			{
				resetsp.GetComponent<Image> ().enabled = true;
			}
			termine = true;
			seguimiento = 0;
			restart.SetActive (true);
			Manager.enjuego = false;
			//SceneManager.LoadScene ("Main");
		}
	}
	/*void setup()
	{
		gameObject.SetActive (false);
	}*/
	public void showCards()
	{
		Debug.Log ("in");
		gameObject.SetActive (true);
	}
}
