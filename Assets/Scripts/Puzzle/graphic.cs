﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class graphic : MonoBehaviour {
	public GameObject inicio,menu,gui,btl,btm,btp,dl,dm,dp, total,resetBt,insp,insm,insl;
	// Use this for initialization
	void Start () {
		setup ();
	}

	// Update is called once per frame
	void Update () {
	
	}
	void setup(){
		inicio.SetActive (false);
		menu.SetActive (false);
		gui.SetActive (false);
		resetBt.SetActive (false);
		insl.SetActive (false);
		insm.SetActive (false);
		insp.SetActive (false);
	}
	public void ShowInicio(){
		inicio.SetActive (true);
		//resetBt.SetActive (false);
	}
	public void ShowMenu(string juego=null){
		setup ();
		menu.SetActive (true);
		switch (juego) {
		case "puzzle":
			btp.SetActive (false);
			dp.SetActive (true);
			break;
		case "memoria":
			btm.SetActive (false);
			dm.SetActive (true);
			break;
		case "laberinto":
			btl.SetActive (false);
			dl.SetActive (true);
			break;
		}
		total.GetComponent<Text> ().text = Main.puntaje.ToString ("f0");
	}
	public void Showgui(string instrucciones = null){
		setup ();
		gui.SetActive (true);
		switch (instrucciones) {
		case "puzzle":
			insp.SetActive (true);
			break;
		case "memoria":
			insm.SetActive (true);
			break;
		case "laberinto":
			insl.SetActive (true);
			break;
		}
	}
	public void ShowResetBt(){
		setup ();
		menu.SetActive (true);
		dp.SetActive (false);
		dm.SetActive (false);
		dl.SetActive (false);

		//menu.SetActive (false);
		resetBt.SetActive (true);
	}
}
