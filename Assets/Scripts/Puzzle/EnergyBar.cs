﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnergyBar : MonoBehaviour 
{
	public float fillamount, timer;
	public Image content;
	public static bool terminado;
	public GameObject reseteng, resetsp, restart;
	// Use this for initialization
	void Start () 
	{
		timer = 30;
		terminado = false;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Timer.tiempoStart == true) 
		{
			timer -= 1 * Time.deltaTime;
			fillamount = timer / 30;
			if (ActivateAnimation.tiempoextra == true) 
			{
				//Debug.Log ("I'm the impossible changing");
				if (timer >= 25) 
				{
					timer = 30;
				}
				if (timer < 25) 
				{
					timer = 25;
				}
			}
			if (ActivateAnimation.menostiempo == true) 
			{
				if (timer >= 10) 
				{
					timer = 10;
					//timer 
				}
				if (timer < 10 && timer >= 4) 
				{
					timer = 5;
					//timer 
				}
				if (timer < 4) {
					timer = 1;
				}
			}
			HandleBar ();
			if (timer <= 0)
			{
				restart.SetActive (true);
				if (reseteng.activeSelf) 
				{
					reseteng.GetComponent<Image> ().enabled = true;
				}
				if (resetsp.activeSelf) 
				{
					resetsp.GetComponent<Image> ().enabled = true;
				}
				terminado = true;
			}
		}
	}

	void HandleBar ()
	{
		content.fillAmount = fillamount;
	}
}
