﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class Puzzlesc : MonoBehaviour{
	static public int tempDrag;
	public static int seguimientoPuzzle = 0;
	public static bool termine = false;
	public GameObject[] piezas = new GameObject[9];
	public Sprite[] frase1 = new Sprite[9];
	public Sprite[] frase2 = new Sprite[9];
	public Sprite[] frase3 = new Sprite[9];
	public Sprite[] frase4 = new Sprite[9];
	public Sprite[] frase5 = new Sprite[9];
	public Sprite[] frase6 = new Sprite[9];
	public Sprite[] frase7 = new Sprite[9];
	public GameObject restart, reseteng, resetsp;
	//public GameObject inst;
	//hecho por Nico
	public AudioSource audios;
	bool flag;

	void Start () {
		termine = false;
		//inst.SetActive (true);
		int numFrase = Random.Range (1, 2);
		switch (numFrase) {
		case 1:
			montarPiezas (frase1);
			audios.Play ();
			break;
		case 2:
			montarPiezas (frase2);
			audios.Play ();
			break;
		case 3:
			montarPiezas (frase3);
			audios.Play ();
			break;
		case 4:
			montarPiezas (frase4);
			audios.Play ();
			break;
		case 5:
			montarPiezas (frase5);
			audios.Play ();
			break;
		case 6:
			montarPiezas (frase6);
			audios.Play ();
			break;
		case 7:
			montarPiezas (frase7);
			audios.Play ();
			break;
		}
		//Setup ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Manager.enjuego == true) 
		{
			if (seguimientoPuzzle == 12) 
			{
				termine = true;
				if (reseteng.activeSelf) 
				{
					reseteng.GetComponent<Image> ().enabled = true;
				}
				if (resetsp.activeSelf) 
				{
					resetsp.GetComponent<Image> ().enabled = true;
				}
                if (!flag)
                {
					StartCoroutine(GoBack());
					flag = true;
				}
			}
		}
	}
	public void ShowPuzzel()
	{
		gameObject.SetActive (true);
	}
	public void cerrarPuzzle()
	{
		gameObject.SetActive (false);
		seguimientoPuzzle = 0;
	}
	private void montarPiezas(Sprite[] frase)
	{
		for (int i = 0; i < piezas.Length; i++) 
		{
			piezas[i].GetComponent<Image> ().sprite = frase[i];
		}
		
	}

	IEnumerator GoBack()
    {
		restart.SetActive(true);
		yield return new WaitForSeconds(1);
		SceneManager.LoadScene(0);
    }
}
