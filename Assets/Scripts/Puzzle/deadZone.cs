﻿using UnityEngine;
using System.Collections;

public class deadZone : MonoBehaviour {
	public Transform spawnpoint;
	public GameObject Marble;
	GameObject ballclone;
	public AudioSource golpe;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnTriggerEnter(Collider other)
	{
		golpe.Play ();
		Destroy (other.gameObject);
		ballclone = Instantiate (Marble, spawnpoint.transform.position, Quaternion.identity) as GameObject;
		ballclone.transform.parent = gameObject.transform.parent;
	}
}
