﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DynamicScenery : MonoBehaviour 
{
	public GameObject mountains, mountains2, clouds, clouds2, clouds3, enemies, powerups, airport;
	public Transform airporttarget;
	public float speed, timer, airportspeed;
	float step;
	Vector3 positions;
	GameObject enemy, powerup; 
	GameObject [] moving;
	//Transform airportorigin;
	Vector3 airportorigin;
	// Use this for initialization
	void Start () 
	{
		airportorigin = airport.transform.position;
		//Invoke ("Instansu", Random.Range (0, 10));
	}
	
	// Update is called once per frame
	void Update () 
	{
		//Debug.Log ("original position" + airportorigin);
		//Debug.Log ("readings from energy bar: " + EnergyBar.terminado);
		if (Timer.tiempoStart == true) 
		{
			step = speed * Time.deltaTime;
			airport.transform.position = Vector3.MoveTowards (airport.transform.position, airporttarget.position, step);
			timer -= 1 * Time.deltaTime;
			positions = new Vector3 (Random.Range (220, 250), Random.Range (-90, 100), 0);
			mountains.transform.Translate (Vector3.left * Time.deltaTime * speed);
			mountains2.transform.Translate (Vector3.left * Time.deltaTime * speed);

			if (timer <= 0f) 
			{
				enemy = Instantiate (enemies, positions, Quaternion.identity) as GameObject;
				enemy.transform.SetParent (GameObject.FindGameObjectWithTag ("canvas").transform, false);
				timer = 5f;
			}
			if (timer >= 1.0f && timer <= 1.02f) 
			{
				powerup = Instantiate (powerups, positions, Quaternion.identity) as GameObject;
				powerup.transform.SetParent (GameObject.FindGameObjectWithTag ("canvas").transform, false);
			}
			if (timer >= 2.0f && timer <= 2.02f) 
			{
				powerup = Instantiate (clouds3, positions, Quaternion.identity) as GameObject;
				powerup.transform.SetParent (GameObject.FindGameObjectWithTag ("canvas").transform, false);
			}
			if (timer >= 3.0f && timer <= 3.02f) 
			{
				powerup = Instantiate (clouds2, positions, Quaternion.identity) as GameObject;
				powerup.transform.SetParent (GameObject.FindGameObjectWithTag ("canvas").transform, false);
			}
			if (timer >= 4.0f && timer <= 4.02f) 
			{
				powerup = Instantiate (clouds, positions, Quaternion.identity) as GameObject;
				powerup.transform.SetParent (GameObject.FindGameObjectWithTag ("canvas").transform, false);
			}
		}
		if (EnergyBar.terminado == true) 
		{
			//Debug.Log ("Let me in");
			airport.transform.position = Vector3.MoveTowards (airport.transform.position, airportorigin, step);
		}
	}
}
