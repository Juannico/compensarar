﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class drop : MonoBehaviour, IDropHandler,IPointerEnterHandler,IPointerExitHandler  {
	public int ActualDrop;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {

	}
	public void OnPointerEnter(PointerEventData e){
	}
	public void OnPointerExit(PointerEventData e){
	}
	public void OnDrop(PointerEventData e){
		if (e.pointerDrag.GetComponent<drag> ().Draggable == true) {
			if (ActualDrop == Puzzlesc.tempDrag) {
				Puzzlesc.seguimientoPuzzle++;
				//Debug.Log ("yes");
				e.pointerDrag.GetComponent<drag> ().CancelDrag ();
			} else {
				//Debug.Log ("no, porque actualdrop es: " + ActualDrop + " y tempdrag es: " + Puzzlesc.tempDrag);
				e.pointerDrag.GetComponent<drag> ().VolverAposicion ();
			}
		}
	}
}
