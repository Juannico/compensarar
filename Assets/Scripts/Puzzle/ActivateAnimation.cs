﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateAnimation : MonoBehaviour 
{
	Animator anim;
	public static bool tiempoextra, menostiempo;
	// Use this for initialization
	void Start () 
	{
		anim = GetComponent<Animator>();
		tiempoextra = false;
		menostiempo = false;
	}
	
	// Update is called once per frame
	void Update () 
	{
		//Debug.Log ("tiempo extra: " + tiempoextra);
		//Debug.Log ("tiempo menos: " + menostiempo);
	}

	void OnCollisionEnter2D (Collision2D col)
	{
		//Debug.Log ("collided");
		if (col.gameObject.tag == "Avion") 
		{
			//Debug.Log (name);
			anim.SetBool ("Crashed", true); 
			if (name.Contains ("Powerup")) 
			{
				//Debug.Log ("I'm the impossible getting in");
				tiempoextra = true;
			} 
			if (name.Contains ("Enemy")) 
			{
				menostiempo = true;
			}
		}
	}
	void OnCollisionExit2D (Collision2D col)
	{
		if (col.gameObject.tag == "Avion") {
			//Debug.Log ("I'm the impossible exiting");
			tiempoextra = false;
			menostiempo = false;
		}
	}
}
