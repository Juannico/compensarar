﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Timer : MonoBehaviour 
{
	public GameObject scoreobj, finalscore;
	//public GameObject timer;
	float segundos = 0;
	public float score;
	public static bool tiempoStart = false;
	public static bool tiempoStop = false;
	public float tiempoJuego;
	[SerializeField] GameObject resetBtn;
	[SerializeField] Text winlose;

	// Use this for initialization
	void Start ()
	{
		tiempoStop = false;
		tiempoStart = false;
		segundos = tiempoJuego;
		//regresiva = 3;
	}

	// Update is called once per frame
	void Update () 
	{
		//Debug.Log (tiempoStart);
		if (tiempoStart == true) 
		{
			segundos -= Time.deltaTime;
			score =  tiempoJuego*2-segundos*2;
			//regresiva -= Time.deltaTime;
		}
		gameObject.GetComponent<Text> ().text = segundos.ToString ("f0");
		scoreobj.GetComponent<Text> ().text = score.ToString ("f0");
		/*if (segundos <= 0 || Memory.termine == true || meta.finished == true || Puzzlesc.termine == true) 
		{
			resetBtn.SetActive(true);
			tiempoStart = false;
			tiempoStop = true;
			finalscore.GetComponent<Text> ().text = score.ToString ("f0");
		}*/
		if (segundos <= 0 && !Memory.termine)
		{
			winlose.text = "Gracias por jugar";
			resetBtn.SetActive(true);
			tiempoStart = false;
			tiempoStop = true;
			finalscore.GetComponent<Text>().text = score.ToString("f0");
		}else if (Memory.termine)
        {
			winlose.text = "Ganaste";
			resetBtn.SetActive(true);
			tiempoStart = false;
			tiempoStop = true;
			finalscore.GetComponent<Text>().text = score.ToString("f0");
		}
		if (EnergyBar.terminado == true) 
		{
			tiempoStart = false;
			tiempoStop = true;
			finalscore.GetComponent<Text> ().text = segundos.ToString ("f0") + " km";
		}
		//Debug.Log ("segundos: " + segundos);
	}

	public void Reset(){
		segundos = 0; 
		//Memory.termine = false;

	}
	public void stop(){
		tiempoStart = false;
	}
}
