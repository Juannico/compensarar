﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Colliders : MonoBehaviour {
	public GameObject generator;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.tag == "mountain") {
			col.transform.SetParent(GameObject.FindGameObjectWithTag ("canvas").transform, false);
			col.transform.position = generator.transform.position;
		}
		if (col.tag == "150") {
			Destroy (col.gameObject);
		}
	}

}
