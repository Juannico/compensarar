﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Joystick : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler
{
	Image background, joystick;
	Vector3 inputVector;
	public static float hor, ver;
	// Use this for initialization
	void Start () 
	{
		background = GetComponent<Image> ();
		joystick = transform.GetChild (0).GetComponent<Image> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		hor = inputVector.x;
		ver = inputVector.z;
	}

	public virtual void OnDrag (PointerEventData ped)
	{
		Vector2 pos;
		if (RectTransformUtility.ScreenPointToLocalPointInRectangle (background.rectTransform, ped.position, ped.pressEventCamera, out pos)) 
		{
			pos.x = (pos.x / background.rectTransform.sizeDelta.x);
			pos.y = (pos.y / background.rectTransform.sizeDelta.y);
			inputVector = new Vector3 (pos.x*2+1, 0, pos.y*2-1);
			inputVector = (inputVector.magnitude > 0.1f) ? inputVector.normalized : inputVector;

			joystick.rectTransform.anchoredPosition = new Vector3 (inputVector.x*background.rectTransform.sizeDelta.x/3, inputVector.z*background.rectTransform.sizeDelta.y/3);
		}
	}

	public virtual void OnPointerDown (PointerEventData ped)
	{
		OnDrag (ped);
	}

	public virtual void OnPointerUp (PointerEventData ped)
	{
		inputVector = Vector3.zero;
		joystick.rectTransform.anchoredPosition = Vector3.zero;
	}

	public float Horizontal () 
	{
		if (inputVector.x != 0) 
		{
			return inputVector.x;
		} 
		else 
		{
			return Input.GetAxis ("Horizontal");
		}
	}
	public float Vertical () 
	{
		if (inputVector.z != 0) 
		{
			return inputVector.z;
		} 
		else 
		{
			return Input.GetAxis ("Vertical");
		}
	}
}

