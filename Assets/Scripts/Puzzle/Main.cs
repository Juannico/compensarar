﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Main : MonoBehaviour {
	public GameObject Gui_Controller;
	public GameObject Puzzel_Controller, Memory_Controller,Labyrinth_Controller;
	public GameObject cronometro;
	string juegoEncurso;
	public int tiempoJuego = 40;
	public static float puntaje = 0;
	int numerodeJuegos = 0;

	// Use this for initialization
	void Start () {
		cronometro.GetComponent<Timer> ().tiempoJuego = tiempoJuego;
		Gui_Controller.GetComponent<graphic> ().ShowInicio ();
	}
	
	// Update is called once per frame
	void Update () {
		/*if (Memory_Controller.GetComponent<Memory> ().termine == true || Puzzel_Controller.GetComponent<Puzzlesc>().termine == true || Labyrinth_Controller.GetComponent<laberint>().termine == true) {
			Memory_Controller.GetComponent<Memory> ().termine = false;
			//numerodeJuegos++;
			Puzzel_Controller.GetComponent<Puzzlesc>().termine = false;
			//numerodeJuegos++;
			//hecho por Nico
			Labyrinth_Controller.GetComponent<laberint>().termine = false;
			//numerodeJuegos++;
			TerminoJuego ();
			//Debug.Log (numerodeJuegos);
			//numerodeJuegos++;
		}
		if (cronometro.GetComponent<Timer> ().tiempoStop == true) {
			cronometro.GetComponent<Timer> ().tiempoStop = false;
			TerminoJuego ();
		}
		if (numerodeJuegos >= 3) {
			Gui_Controller.GetComponent<graphic> ().ShowResetBt ();
		}*/
	}
	public void EventoToque(string elemento){
		switch (elemento) {
		case "btinicio":
			Gui_Controller.GetComponent<graphic> ().ShowMenu ();
			break;
		case "btMemoria":
			SceneManager.LoadScene ("Memory");
			//Debug.Log ("memoria");
			juegoEncurso = "memoria";
			Gui_Controller.GetComponent<graphic> ().Showgui (juegoEncurso);
			Memory_Controller.GetComponent<Memory> ().showCards ();
			cronometro.GetComponent<Timer> ().Reset ();
			break;
		case "btPuzzle":
			SceneManager.LoadScene ("Puzzle");
			juegoEncurso = "puzzle";
			Gui_Controller.GetComponent<graphic> ().Showgui (juegoEncurso);
			Puzzel_Controller.GetComponent<Puzzlesc> ().ShowPuzzel ();
			cronometro.GetComponent<Timer> ().Reset ();
			break;
		case "btLaberinto":
			SceneManager.LoadScene ("Maze");
			//ball.enJuego = true;
			juegoEncurso = "laberinto";
			Gui_Controller.GetComponent<graphic> ().Showgui (juegoEncurso);
			Labyrinth_Controller.GetComponent<laberint> ().showLabyrinth ();
			cronometro.GetComponent<Timer> ().Reset ();
			break;
		case "btAvion":
			SceneManager.LoadScene ("Plane");
			//ball.enJuego = true;
			juegoEncurso = "laberinto";
			Gui_Controller.GetComponent<graphic> ().Showgui (juegoEncurso);
			Labyrinth_Controller.GetComponent<laberint> ().showLabyrinth ();
			cronometro.GetComponent<Timer> ().Reset ();
			break;
		case "btReset":
			puntaje = 0;
			SceneManager.LoadScene ("Main");
			break;
		}
	}
	public void TerminoJuego(){
		numerodeJuegos++;
		cronometro.GetComponent<Timer> ().stop ();
		puntaje += cronometro.GetComponent<Timer> ().score;
		Puzzel_Controller.GetComponent<Puzzlesc> ().cerrarPuzzle ();
		Gui_Controller.GetComponent<graphic> ().ShowMenu (juegoEncurso);
		Memory_Controller.SetActive (false);
		Labyrinth_Controller.SetActive (false);
		//ball.enJuego = false;
		//Debug.Log (puntaje);
	}
	public void cerrarInstrucciones(GameObject clicker){
		clicker.SetActive (false);
		//cronometro.GetComponent<Timer> ().tiempoStart = true;
	}
}
