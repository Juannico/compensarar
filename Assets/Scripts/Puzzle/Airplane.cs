﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Airplane : MonoBehaviour {
	public float speed = 1.5f;
	float lockpos = 0;
	Vector3 originalplanepose;
	float step;
	// Use this for initialization
	void Start () 
	{
		originalplanepose = transform.position;
	}
	
	// Update is called once per frame
	void Update () 
	{
		step = speed * Time.deltaTime;
		if (Timer.tiempoStart == true) 
		{
			transform.rotation = Quaternion.Euler (transform.rotation.eulerAngles.x, lockpos, lockpos);
			if (Input.GetKey (KeyCode.LeftArrow)) 
			{
				transform.position += Vector3.left * speed * Time.deltaTime;
			}
			if (Input.GetKey (KeyCode.RightArrow)) 
			{
				transform.position += Vector3.right * speed * Time.deltaTime;
			}
			if (Input.GetKey (KeyCode.UpArrow)) 
			{
				transform.position += Vector3.up * speed * Time.deltaTime;
			}
			if (Input.GetKey (KeyCode.DownArrow)) 
			{
				transform.position += Vector3.down * speed * Time.deltaTime;
			}
		}
		if (Timer.tiempoStop == true) 
		{
			transform.rotation = Quaternion.Euler (transform.rotation.eulerAngles.x, lockpos, lockpos);
			transform.position = Vector3.MoveTowards (transform.position, originalplanepose, step);
		}
	}

	void FixedUpdate ()
	{
		Physics2D.IgnoreLayerCollision (8, 9);
		Physics2D.IgnoreLayerCollision (9, 10);
	}
}
