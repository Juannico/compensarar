﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class drag : MonoBehaviour,IBeginDragHandler , IDragHandler, IEndDragHandler {
	public int DragActual;
	public bool Draggable = true;
	Vector2 positionIni;
	public GameObject target1, target2, target3, target4, target5, target6, target7, target8, target9, target10, target11, target12;
	/*Vector2[] ArrayPosition = {new Vector2(243.1f,543.8f), new Vector2(350.2f,543.8f), new Vector2(445f,529.5f), 
		new Vector2(222.8f,451.5f), new Vector2(333.8f,433f), new Vector2(445f,416.2f), 
		new Vector2(220.7f,338.7f), new Vector2(319.6f,321.8f), new Vector2(430f,324.0f)};*/
	// Use this for initialization
	void Start () {
		positionIni = this.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void OnBeginDrag(PointerEventData e)
	{
		if (Draggable == true) {
			this.GetComponent<CanvasGroup> ().blocksRaycasts = false;
			Puzzlesc.tempDrag = DragActual;
			gameObject.transform.SetAsLastSibling ();
		}
	}
	public void OnDrag(PointerEventData e)
	{
		if (Draggable == true) {
			gameObject.transform.position = Input.mousePosition;
		}
	}

	public void OnEndDrag(PointerEventData e)
	{
		this.GetComponent<CanvasGroup> ().blocksRaycasts = true;
		Puzzlesc.tempDrag = 0;
	}
	public void CancelDrag()
	{
		Draggable = false;
		//this.transform.position = ArrayPosition[DragActual-1];
		switch (DragActual) 
		{
			case 1:
				this.transform.position = target1.transform.position;
				break;
			case 2:
				this.transform.position = target2.transform.position;
				break;
			case 3:
				this.transform.position = target3.transform.position;
				break;
			case 4:
				this.transform.position = target4.transform.position;
				break;
			case 5:
				this.transform.position = target5.transform.position;
				break;
			case 6:
				this.transform.position = target6.transform.position;
				break;
			case 7:
				this.transform.position = target7.transform.position;
				break;
			case 8:
				this.transform.position = target8.transform.position;
				break;
			case 9:
				this.transform.position = target9.transform.position;
				break;
		case 10:
			this.transform.position = target10.transform.position;
			break;
		case 11:
			this.transform.position = target11.transform.position;
			break;
		case 12:
			this.transform.position = target12.transform.position;
			break;
		}
	}
	public void VolverAposicion(){
		this.transform.position = positionIni;
	}
}
