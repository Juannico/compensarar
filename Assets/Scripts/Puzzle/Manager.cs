﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Manager : MonoBehaviour 
{
	public GameObject /*language, instrucciones,*/ timer, countdown, fondo/*, reseteng, resetsp*/;
	//public GameObject[] engtext, sptext;
	public int tiempoJuego = 600;
	float countdownvalue;
	//public int tiempoMaximo = 600;//10 minutos
	public static float puntaje = 0;
	public static bool enjuego = false;
	//float pos;
	Vector3 temp;

	private void Awake()
	{
		timer.GetComponent<Timer>().tiempoJuego = tiempoJuego;
	}
    // Use this for initialization
    void Start () 
	{
		temp = fondo.transform.position;
		temp.z = 50;
		fondo.transform.position = temp;
		//Debug.Log (temp);
		//engtext = GameObject.FindGameObjectsWithTag ("english");
		//sptext = GameObject.FindGameObjectsWithTag ("spanish");
		//language.SetActive (true);
		//Debug.Log (language.activeSelf);
		//instrucciones.SetActive (false);
		countdown.SetActive (false);

		countdownvalue = 3;
		//countdown.GetComponent<Text>().text
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (countdown.activeSelf == true) 
		{
			countdown.GetComponent<Text> ().text = countdownvalue.ToString("f0");
			countdownvalue -= Time.deltaTime;
			//Debug.Log (countdownvalue);
			if (countdownvalue <= 0) 
			{
				//Debug.Log ("in");
				countdown.SetActive (false);
				Timer.tiempoStart = true;
				temp.z = 100;
				fondo.transform.position = temp;
			}
		}
	}

	public void EventoToque(string elemento)
	{
		switch (elemento) {
		case "btMemoria":
			SceneManager.LoadScene ("Memory");
			break;
		case "btPuzzle":
			SceneManager.LoadScene ("Puzzle");
			break;
		case "playPuzzle":
			Debug.Log ("click");
			GameObject.FindGameObjectWithTag ("instruc").SetActive (false);
			countdown.SetActive (true);
			enjuego = true;
			//GameObject.Find ("Cardholder").GetComponent<Memory> ().showCards ();
			//Memory_Controller.GetComponent<Memory> ().showCards ();

			break;
		case "btLaberinto":
			SceneManager.LoadScene ("Maze");
			//ball.enJuego = true;
			break;
		case "btAvion":
			SceneManager.LoadScene ("Plane");
			break;
		case "btReset":
			//puntaje = 0;
			//SceneManager.LoadScene ("Main");
			Memory.termine = false;
			Puzzlesc.termine = false;
			Puzzlesc.seguimientoPuzzle = 0;
			SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
			break;
		/*case "bnEnglish":
			for (int i = 0; i < engtext.Length; i++) {
				engtext [i].SetActive (true);
				sptext [i].SetActive (false);
			}
			//language.SetActive (false);
			//instrucciones.SetActive (true);
			//reseteng.SetActive (true);
			break;
		case "bnSpanish":
			for (int i = 0; i < sptext.Length; i++) {
				sptext [i].SetActive (true);
				//engtext [i].SetActive (false);
			}
			//language.SetActive (false);
			//instrucciones.SetActive (true);
			//resetsp.SetActive (true);
			break;*/
		}
	}
}
